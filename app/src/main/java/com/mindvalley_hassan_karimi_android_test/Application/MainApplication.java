package com.mindvalley_hassan_karimi_android_test.Application;

import android.app.Application;

import com.mindvalley_hassan_karimi_android_test.Cache.Queue.QueueManager;

/**
 * Created by sepronik on 07/13/2016.
 */
public class MainApplication extends Application {

    private QueueManager queueManager;

    private static MainApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        queueManager = new QueueManager();

    }

    public static synchronized MainApplication getInstance() {
        return mInstance;
    }


    public QueueManager getRequestQueue() {
        if (queueManager == null) {
            queueManager = new QueueManager();//(getApplicationContext());
        }
        return queueManager;
    }


}

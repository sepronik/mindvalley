package com.mindvalley_hassan_karimi_android_test.PinBoard;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardUser {

    private String id;
    private String userName;
    private String name;

    private PinboardUserProfileImage profileImage;


    public PinboardUser(String id ,String userName ,String name , PinboardUserProfileImage profileImage){
        this.id = id ;
        this.userName = userName ;
        this.name = name ;
        this.profileImage = profileImage;
    }


    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PinboardUserProfileImage getProfileImage() {
        return profileImage;
    }

    public String getUserName() {
        return userName.trim();
    }
}

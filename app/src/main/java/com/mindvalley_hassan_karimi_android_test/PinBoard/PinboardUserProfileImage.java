package com.mindvalley_hassan_karimi_android_test.PinBoard;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardUserProfileImage {

    private String small;
    private String medium;
    private String large;

    public PinboardUserProfileImage(String small , String medium ,String large){
        this.small = small ;
        this.medium = medium ;
        this.large = large ;
    }

    public String getLarge() {
        return large;
    }

    public String getMedium() {
        return medium;
    }

    public String getSmall() {
        return small;
    }
}

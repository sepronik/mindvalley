package com.mindvalley_hassan_karimi_android_test.PinBoard;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardCategoriesItem {

    private String id;
    private String title;

    public PinboardCategoriesItem(String id , String title){
        this.id = id;
        this.title = title ;

    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }
}

package com.mindvalley_hassan_karimi_android_test.PinBoard.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sepronik on 07/14/2016.
 */
public class ConvertDate {

    private static final String fermatDate = "yyyy-MM-dd'T'HH:mm:ss-SS:SS";


    public static Date getDateObjectFromDateString(String dateStr) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(fermatDate);
            return format.parse(dateStr);
        }
        catch (Exception e){
        }
        return  new Date();
    }

}

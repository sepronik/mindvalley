package com.mindvalley_hassan_karimi_android_test.PinBoard;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindvalley_hassan_karimi_android_test.Application.MainApplication;
import com.mindvalley_hassan_karimi_android_test.Cache.Loader.ImageLoader;
import com.mindvalley_hassan_karimi_android_test.R;

/**
 * Created by sepronik on 07/14/2016.
 */

public class PinboardAdapter extends RecyclerView.Adapter<PinboardAdapter.PinboardViewHolder> {

    private PinboardList pinboardList;

    private ImageLoader imageLoader;

    public PinboardAdapter() {
        pinboardList = new PinboardList();
        imageLoader = new ImageLoader(MainApplication.getInstance().getRequestQueue());

    }

    @Override
    public int getItemCount() {
        return pinboardList.size();
    }

    public void addItemToFirstList(PinboardList pinboardList) {
        this.pinboardList.addAll(0,pinboardList);
        notifyDataSetChanged();
    }

    public void addItemToEndList(PinboardList pinboardList) {
        this.pinboardList.addAll(pinboardList);
        notifyDataSetChanged();
    }


    @Override
    public PinboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pinboard_list_item, parent, false);

        return new PinboardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PinboardViewHolder holder, int position) {
        PinboardItem item = pinboardList.get(position);

        imageLoader.setImageToView(item.getUrlImage(), holder.pinboardMainImage);
        imageLoader.setImageToView(item.getUserUrlImage(), holder.pinboardUserImage);
        holder.pinboardUserText.setText(item.getUserName());

        if(item.getCatsSize() > 0) {
            holder.pinboardCatsText.setText(item.getCatsNames());
            holder.pinboardCatsLayout.setVisibility(View.VISIBLE);
        }
        else
            holder.pinboardCatsLayout.setVisibility(View.INVISIBLE);

    }

    public class PinboardViewHolder extends RecyclerView.ViewHolder {
        public ImageView pinboardMainImage;
        public ImageView pinboardUserImage;
        public TextView pinboardCatsText;
        public TextView pinboardUserText;
        public View pinboardCatsLayout;

        public PinboardViewHolder(View view) {
            super(view);
            pinboardMainImage = (ImageView) view.findViewById(R.id.pinBoardMainImage);
            pinboardUserImage = (ImageView) view.findViewById(R.id.pinBoardUserImage);
            pinboardCatsText = (TextView) view.findViewById(R.id.pinBoardCatsText);
            pinboardCatsLayout =  view.findViewById(R.id.pinBoardCatsLayout);
            pinboardUserText = (TextView) view.findViewById(R.id.pinBoardUserName);

        }
    }

}
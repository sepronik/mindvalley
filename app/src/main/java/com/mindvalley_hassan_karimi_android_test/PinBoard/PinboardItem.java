package com.mindvalley_hassan_karimi_android_test.PinBoard;

import java.util.Date;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardItem {

    private String id;
    private Date createdAt ;
    private boolean likedByUser ;
    private int countOfLike ;
    private PinboardUser user;
    private PinboardCategoriesList categoriesList;
    private PinboardUrls urls;

    public PinboardItem(String id , Date createdAt, boolean likedByUser ,int countOfLike,
                                    PinboardUser user , PinboardCategoriesList categoriesList ,
                                    PinboardUrls urls) {
        this.id = id;
        this.createdAt = createdAt;
        this.likedByUser = likedByUser ;
        this.countOfLike = countOfLike ;
        this.user = user;
        this.urls = urls;
        this.categoriesList = categoriesList;
    }

    public String getId() {
        return id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public int getCountOfLike() {
        return countOfLike;
    }

    public PinboardUser getUser() {
        return user;
    }

    public PinboardUrls getUrls() {
        return urls;
    }

    //TODO check and send correct image url .
    public String getUrlImage() {
        return urls.getSmall();
    }

    //TODO check and send correct image url .
    public String getUserUrlImage() {
        return user.getProfileImage().getSmall();
    }

    public String getUserName() {
        return user.getName();
    }

    public String getCatsNames() {
        return categoriesList.getNames();
    }


    public int getCatsSize() {
        return categoriesList.size();
    }

}

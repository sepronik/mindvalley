package com.mindvalley_hassan_karimi_android_test.PinBoard.Parser;

import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardCategoriesItem;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardCategoriesList;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardItem;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardList;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardUrls;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardUser;
import com.mindvalley_hassan_karimi_android_test.PinBoard.PinboardUserProfileImage;
import com.mindvalley_hassan_karimi_android_test.PinBoard.Util.ConvertDate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardJsonParse {


    private final static String PinId = "id" ;
    private final static String PinCreatedAt = "created_at" ;
    private final static String PinLikes = "liked_by_user" ;
    private final static String PinCountLikes = "likes" ;
    private final static String PinCategories = "categories" ;
    private final static String PinUrls = "urls" ;
    private final static String PinUser = "user" ;

    private final static String CategoriesId = "id" ;
    private final static String CategoriesTitle = "title" ;

    private final static String UserProfileImageSmall = "small" ;
    private final static String UserProfileImageMedium = "medium" ;
    private final static String UserProfileImageLarge = "large" ;

    private final static String UserId = "id" ;
    private final static String UserUserName = "username" ;
    private final static String UserName = "name" ;
    private final static String UserProfileImage = "profile_image" ;

    private final static String UrlsSmall = "small" ;


    public static PinboardList getListPins(JSONArray arrayPins) throws JSONException{
        PinboardList pinboardList = new PinboardList();

        for (int i = 0;i< arrayPins.length() ; i++)
            pinboardList.add(getPinItem(arrayPins.getJSONObject(i)));

        return pinboardList;
    }


    private static PinboardItem getPinItem(JSONObject pin)throws JSONException {
        return new PinboardItem(
                pin.getString(PinId),
                ConvertDate.getDateObjectFromDateString(pin.getString(PinCreatedAt)),
                pin.getBoolean(PinLikes),
                pin.getInt(PinCountLikes),
                getUser(pin.getJSONObject(PinUser)),
                getCategoriesList(pin.getJSONArray(PinCategories)),
                getPinUrls(pin.getJSONObject(PinUrls))

        ) ;
    }

    private static PinboardUser getUser(JSONObject pinUser)throws JSONException{
        return new PinboardUser(
                pinUser.getString(UserId),
                pinUser.getString(UserUserName),
                pinUser.getString(UserName),
                getUserProfileImage( pinUser.getJSONObject(UserProfileImage))
        );
    }

    private static PinboardUserProfileImage getUserProfileImage(JSONObject profileImages)throws JSONException{
        return new PinboardUserProfileImage(
                profileImages.getString(UserProfileImageSmall),
                profileImages.getString(UserProfileImageMedium),
                profileImages.getString(UserProfileImageLarge)
        );
    }

    private static PinboardCategoriesList getCategoriesList(JSONArray cats)throws JSONException{
        PinboardCategoriesList categoriesList = new PinboardCategoriesList();

        for (int i = 0;i< cats.length() ; i++)
            categoriesList.add(getCategoriesItem(cats.getJSONObject(i)));

        return categoriesList;
    }

    private static PinboardCategoriesItem getCategoriesItem(JSONObject catItem)throws JSONException{
        return  new PinboardCategoriesItem(
                catItem.getString(CategoriesId),
                catItem.getString(CategoriesTitle)
        );
    }

    private static PinboardUrls getPinUrls(JSONObject urls)throws JSONException{
        return  new PinboardUrls(
                urls.getString(UrlsSmall)
        );
    }
}

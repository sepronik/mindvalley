package com.mindvalley_hassan_karimi_android_test.PinBoard;

import java.util.ArrayList;

/**
 * Created by sepronik on 07/14/2016.
 */
public class PinboardCategoriesList extends ArrayList<PinboardCategoriesItem>{

    private final String separate = " AND ";

    public String getNames(){
        String names = "";
        for (int i=0; i<size(); i++) {
            if(i>0)
                names += separate ;
            names += get(i).getTitle();
        }
        return names;
    }
}

package com.mindvalley_hassan_karimi_android_test.PinBoard;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;


import com.mindvalley_hassan_karimi_android_test.Application.MainApplication;
import com.mindvalley_hassan_karimi_android_test.Cache.JSonArray.JSonArrayInterface;
import com.mindvalley_hassan_karimi_android_test.Cache.JSonArray.RequestJSonArray;
import com.mindvalley_hassan_karimi_android_test.PinBoard.Parser.PinboardJsonParse;
import com.mindvalley_hassan_karimi_android_test.PinBoard.Util.ScrollListenerRecyclerView;
import com.mindvalley_hassan_karimi_android_test.R;

import org.json.JSONArray;

public class PinboardActivity extends AppCompatActivity {

    private SwipeRefreshLayout swipeContainer;
    private PinboardAdapter pinboardAdapter;
    private RecyclerView recyclerView;
    private boolean loadData = false;
    private Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setToolBar();

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                fetchAsync();
            }
        });

        swipeContainer.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light);

        setRecyclerViewConfig();
        fetchAsync();
    }

    private void setRecyclerViewConfig(){
        pinboardAdapter = new PinboardAdapter();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(pinboardAdapter);

        recyclerView.setOnScrollListener(new ScrollListenerRecyclerView() {
            @Override
            public void onHide() {
                hideToolBar();
            }
            @Override
            public void onShow() {
                showToolBar();
            }
        });
    }

    private void hideToolBar() {
        //toolbar.animate().translationY(-toolbar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showToolBar() {
        //toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

    private void setToolBar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    private void fetchAsync() {
        if (loadData)
            return;

        loadingStart();

        MainApplication.getInstance().getRequestQueue().addRequest(
                new RequestJSonArray("http://pastebin.com/raw/wgkJgazE", new JSonArrayInterface() {
                    @Override
                    public void onDelivered(JSONArray jsonArray) {
                        try {
                            pinboardAdapter.addItemToFirstList(
                                    PinboardJsonParse.getListPins(jsonArray));
                        }
                        catch (Exception e){
                            Toast.makeText(getApplicationContext() , "Error Load Data" ,
                                    Toast.LENGTH_LONG).show();
                        }
                        finally {
                            loadingFinished();
                        }
                    }

                    @Override
                    public void onError(String error) {
                        Toast.makeText(getApplicationContext() , "Error Load Data" ,
                                Toast.LENGTH_LONG).show();
                        loadingFinished();
                    }
                })
        );
    }

    private void loadingStart() {
        loadData = true;
    }

    private void loadingFinished() {
        swipeContainer.setRefreshing(false);
        loadData = false;
    }
}

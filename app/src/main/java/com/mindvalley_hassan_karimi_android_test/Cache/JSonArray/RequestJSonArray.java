package com.mindvalley_hassan_karimi_android_test.Cache.JSonArray;

import com.mindvalley_hassan_karimi_android_test.Cache.Manager.CacheManager;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Request;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

import org.json.JSONArray;

/**
 * Created by sepronik on 07/13/2016.
 */

public class RequestJSonArray extends Request {


    private JSonArrayInterface requestInterface = null ;


    private JSONArray jsonArray = null;

    public RequestJSonArray(String url  , JSonArrayInterface requestInterface ){
        super(url );
        this.requestInterface = requestInterface;
    }

    @Override
    public void setResponse(Response response) {
        try {
            jsonArray = ((ResponseJSonArray)response).getJsonArray();
        }
        catch (Exception e){
            isError = true;
        }
    }

    @Override
    public Response setData(byte[] data ) {
        return new ResponseJSonArray(getJSonArrayFromByte(data) , data.length);
    }

    private JSONArray getJSonArrayFromByte(byte[] data) {
        try {
            jsonArray = new JSONArray(new String(data));

            return  jsonArray;
        }
        catch (Exception e){
            isError = true;
        }
       return jsonArray;
    }

    @Override
    public void finish() {
        if(requestInterface != null) {
            if (!isError)
                requestInterface.onDelivered(jsonArray);
            else
                requestInterface.onError("Error On Load JSON Array");
        }
    }
}

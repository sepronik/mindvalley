package com.mindvalley_hassan_karimi_android_test.Cache.JSonArray;


import org.json.JSONArray;

/**
 * Created by sepronik on 07/13/2016.
 */
public interface JSonArrayInterface {

    public void onDelivered(JSONArray jsonArray);

    public void onError(String error);

}

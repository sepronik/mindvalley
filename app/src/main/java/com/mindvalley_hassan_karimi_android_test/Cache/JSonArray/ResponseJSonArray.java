package com.mindvalley_hassan_karimi_android_test.Cache.JSonArray;


import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

import org.json.JSONArray;

/**
 * Created by sepronik on 07/13/2016.
 */
public class ResponseJSonArray extends Response {

    JSONArray jsonArray ;
    public ResponseJSonArray(JSONArray jsonArray , int size){
        this.jsonArray = jsonArray;
        setSize(size);
    }

    public JSONArray getJsonArray() {
        updateLastTimeAccess();
        return jsonArray;
    }
}

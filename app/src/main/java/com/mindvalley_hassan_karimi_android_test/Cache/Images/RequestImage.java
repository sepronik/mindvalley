package com.mindvalley_hassan_karimi_android_test.Cache.Images;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.mindvalley_hassan_karimi_android_test.Cache.Manager.CacheManager;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Request;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

/**
 * Created by sepronik on 07/13/2016.
 */
public class RequestImage extends Request {


    private ImageInterface requestInterface = null ;

    private int defaultLoadingImageId = -1;

    private int errorLoadImageId = -1;

    private Bitmap bitmap= null;
    String uu ;

    public RequestImage(String url  ,  ImageInterface requestInterface ){
        super(url );
        uu = url;
        this.requestInterface = requestInterface;
    }

    public RequestImage(String url , int defaultLoadingImageId,  int errorLoadImageId , ImageInterface requestInterface ){
        super(url );
        this.requestInterface = requestInterface;
        this.defaultLoadingImageId = defaultLoadingImageId;
        this.errorLoadImageId = errorLoadImageId;
    }


    @Override
    public void setResponse(Response response) {
        setBitmap(((ResponseImage)response).getBitmap());
    }

    @Override
    public Response setData(byte[] data  ) {
        return new ResponseImage(getBitmapFromByte(data) , data.length);
    }

    private void setBitmap(Bitmap bb) {
        bitmap = bb;
    }

    private Bitmap getBitmapFromByte(byte[] data) {
        try {


            BitmapFactory.Options decodeOptions = new BitmapFactory.Options();

            decodeOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(data, 0, data.length, decodeOptions);
            decodeOptions.inJustDecodeBounds = false;
            bitmap =  BitmapFactory.decodeByteArray(data, 0, data.length, decodeOptions);

            //cacheManager.put(uu , bitmap);

            return  bitmap;
        }
        catch (Exception e){
            isError = true;
        }
       return bitmap;
    }

    @Override
    public void finish() {
        if(requestInterface != null) {
            if (!isError)
                requestInterface.onDelivered(bitmap);
            else
                requestInterface.onError("Error On Load Image");
        }
    }
}

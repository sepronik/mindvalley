package com.mindvalley_hassan_karimi_android_test.Cache.Images;

import android.graphics.Bitmap;

/**
 * Created by sepronik on 07/13/2016.
 */
public interface ImageInterface {

    public void onDelivered(Bitmap bmp);

    public void onError(String error);

}

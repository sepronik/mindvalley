package com.mindvalley_hassan_karimi_android_test.Cache.Images;

import android.graphics.Bitmap;

import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

/**
 * Created by sepronik on 07/13/2016.
 */
public class ResponseImage extends Response {

    Bitmap bitmap ;
    public ResponseImage(Bitmap bitmap , int size ){
        this.bitmap = bitmap;
        setSize(size);
    }

    public Bitmap getBitmap() {
        updateLastTimeAccess();
        return bitmap;
    }

}

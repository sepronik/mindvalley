package com.mindvalley_hassan_karimi_android_test.Cache.Loader;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.mindvalley_hassan_karimi_android_test.Cache.Images.ImageInterface;
import com.mindvalley_hassan_karimi_android_test.Cache.Images.RequestImage;
import com.mindvalley_hassan_karimi_android_test.Cache.Queue.QueueManager;

/**
 * Created by sepronik on 07/14/2016.
 */
public class ImageLoader {

    private QueueManager queueManager;
    public ImageLoader(QueueManager queueManager){
        this.queueManager = queueManager;
    }

    public void setImageToView(String url ,final ImageView imageView){
        imageView.setImageBitmap(null);
        queueManager.addRequest(new RequestImage(url, new ImageInterface() {
            @Override
            public void onDelivered(Bitmap bmp) {
                if(imageView != null)
                    imageView.setImageBitmap(bmp);
            }

            @Override
            public void onError(String error) {

            }
        }));
    }
}

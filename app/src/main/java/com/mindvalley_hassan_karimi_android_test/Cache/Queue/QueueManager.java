package com.mindvalley_hassan_karimi_android_test.Cache.Queue;

import com.mindvalley_hassan_karimi_android_test.Cache.Manager.CacheManager;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Request;
import com.mindvalley_hassan_karimi_android_test.Cache.Downloader.DownloadRequest;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

import org.apache.http.client.HttpClient;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import android.net.http.AndroidHttpClient;

/**
 * Created by sepronik on 07/13/2016.
 */
public class QueueManager  {

    private static final int poolSize = 4;

    private AtomicInteger queueSpecialId = new AtomicInteger();

    private CacheManager cacheManager;

    private DownloadRequest downloadRequest[];

    private final Map<String,Request<?>> queueWaiting = new HashMap<String,Request<?>>();

    private final Map<String,Request<?>> queueCurrent = new HashMap<String,Request<?>>();

    private String userAgent = "mindvalley/0";

    private HttpClient mClient;


    public QueueManager(){
        cacheManager = new CacheManager();
        mClient =  AndroidHttpClient.newInstance(userAgent) ;
        downloadRequest = new DownloadRequest[poolSize];

    }


    public int addRequest(Request  request){
        synchronized (queueWaiting) {
            queueWaiting.put(request.getUrl() , request);
        }
        request.setUniqueId(getNextId());

        nextRequestExecute();

        return  request.getUniqueId();
    }

    public void cancelRequest(int id){
        synchronized (queueCurrent) {
            for (int i = 0; i < poolSize; i++) {
                if (downloadRequest[i] != null) {
                    if (downloadRequest[i].getUniqueId() == id) {
                        downloadRequest[i].stop();
                        queueWaiting.remove(downloadRequest[i].getRequest().getUrl());
                        downloadRequest[i] = null;

                    }
                }
            }
        }
        nextRequestExecute();
    }

    public int getNextId() {
        return queueSpecialId.incrementAndGet();
    }


    public void finishRequest(Request request){
        synchronized (queueCurrent) {
            queueCurrent.remove(request.getUrl());

            for (int i = 0; i < poolSize; i++) {
                if (downloadRequest[i] != null) {
                    if (downloadRequest[i].getUniqueId() == request.getUniqueId()) {
                        downloadRequest[i] = null;
                    }
                }
            }
        }
        nextRequestExecute();
    }

    private synchronized void nextRequestExecute(){
        if(queueCurrent.size() >= poolSize){
            return;
        }

        for (Map.Entry<String,Request<?>> entry : queueWaiting.entrySet()) {
            String key = entry.getKey();
            Request<?> value = entry.getValue();

             if(queueCurrent.get(key) == null){
                 queueCurrent.put(key , value);
                 queueWaiting.remove(key);

                 for (int i = 0 ; i < poolSize ; i++) {
                    if (downloadRequest[i] == null) {
                        DownloadRequest dr = new DownloadRequest(mClient,cacheManager , this );
                        dr.setRequest(value);
                        downloadRequest[i] = dr;
                        downloadRequest[i].start();
                        break;
                     }
                 }
                 break;
             }
        }
    }


}

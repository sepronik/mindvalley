package com.mindvalley_hassan_karimi_android_test.Cache.Downloader;

import com.mindvalley_hassan_karimi_android_test.Cache.Manager.CacheManager;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Request;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;
import com.mindvalley_hassan_karimi_android_test.Cache.Queue.QueueManager;

import android.os.Handler;
import android.os.Looper;
import android.os.Process;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Created by sepronik on 07/13/2016.
 */
public class DownloadRequest extends Thread{

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private Request request;

    private HttpClient mClient;

    private CacheManager cacheManager;

    private QueueManager queueManager;

    private boolean isRunning = false ;

    public DownloadRequest(HttpClient mClient , CacheManager cacheManager , QueueManager queueManager) {
        this.mClient = mClient;
        this.cacheManager = cacheManager;
        this.queueManager = queueManager;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Request getRequest() {
        return request;
    }

    public int getUniqueId() {
        return request.getUniqueId();
    }


    @Override
    public void run() {
        isRunning = true;
        Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
        try {
            Response<?> response;

            response = cacheManager.get(request.getUrl());
            if(response != null){
                request.setResponse(response);
                mHandler.post(mRunnable);
                return;
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            getHttpResponse(createHttpRequest(request)).getEntity().writeTo(baos);

            response = request.setData(baos.toByteArray() );

            if(request.isCacheResponse())
                cacheManager.put(request.getUrl() , response);
        }
        catch (Exception e)  {
            request.setError(true);
        }

        mHandler.post(mRunnable);
    }

    private Runnable  mRunnable = new Runnable() {
        @Override
        public void run() {
            request.finish();
            queueManager.finishRequest(request);
            isRunning = false;
        }
    };

    public boolean isRunning() {
        return isRunning;
    }

    private HttpResponse getHttpResponse(HttpUriRequest httpRequest ) throws IOException{
        HttpParams httpParams = httpRequest.getParams();
        int timeoutMs = request.getTimeOut();
        HttpConnectionParams.setConnectionTimeout(httpParams, request.getTimeOut());
        HttpConnectionParams.setSoTimeout(httpParams, timeoutMs);
        return mClient.execute(httpRequest);
    }

    static HttpUriRequest createHttpRequest(Request<?> request){
        return new HttpGet(request.getUrl());
    }
}

package com.mindvalley_hassan_karimi_android_test.Cache.Manager;


import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by sepronik on 07/13/2016.
 */

public class CacheManager    {

    private LinkedHashMap<String,Response<?>> map;

    private int maxSize = 0;

    private int usedSize = 0;

    public CacheManager(){
        this((int)(Runtime.getRuntime().maxMemory() / 1024) / 8);
    }

    public CacheManager(int maxSize){
        this.maxSize = maxSize;
        this.map = new LinkedHashMap<String, Response<?>>(0, 0.75f, true);
    }

    public int getMaxSize() {
        return maxSize;
    }

    public Response get(String key ) {
        return map.get(KeyHash.getUrlHash(key));
    }

    public synchronized void put(String key , Response response ) {

        int newCacheSize = response.getSize() ;
        if(newCacheSize > maxSize)
            return;

        synchronized (map) {
            if (newCacheSize + usedSize > maxSize)
                removeLastData(newCacheSize);

            usedSize += newCacheSize;
            map.put(KeyHash.getUrlHash(key), response);
        }
    }


    private void removeLastData(int newCacheSize) {
        while (true){
            if(map.size() == 0 || newCacheSize + usedSize < maxSize)
                return;

            String keyRemove = "";
            long minimumTime = System.currentTimeMillis();

            for (Map.Entry<String, Response<?>> entry : map.entrySet()) {
                String key = entry.getKey();
                Response<?> value = entry.getValue();

                long lastTimeUsed = value.getLastTimeAccess();
                if (minimumTime > lastTimeUsed) {
                    keyRemove = key;
                    minimumTime = lastTimeUsed;
                }
            }
            remove(keyRemove);
        }
    }

    private void remove(String key) {
        Response response = map.remove(key);
        usedSize -= response.getSize();
    }

}

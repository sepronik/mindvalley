package com.mindvalley_hassan_karimi_android_test.Cache.Manager;

/**
 * Created by sepronik on 07/13/2016.
 */
public abstract class Request<T> {

    protected int uniqueId;

    protected String url;

    protected int timeOut ;

    static final int defaultTimeOut = 5000 ;

    static boolean cacheResponse = true;

    protected boolean isError = false;

    abstract public Response<T> setData(byte[] data );

    abstract public void finish();

    abstract public void setResponse(Response<T> response);

    public Request(String url){
        this( url , defaultTimeOut ,cacheResponse);
    }

    public Request(String url  ,boolean cacheResponse ){
        this( url , defaultTimeOut , cacheResponse);
    }

    public Request( String url , int timeOut , boolean cacheResponse ){
        this.cacheResponse = cacheResponse;
        this.url = url;
        this.timeOut = timeOut ;
    }

    public int getUniqueId() {
        return uniqueId;
    }

    public int getTimeOut() {
        return timeOut;
    }

    public String getUrl() {
        return url;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public boolean isCacheResponse() {
        return cacheResponse;
    }

    public void setUniqueId(int uniqueId) {
        this.uniqueId = uniqueId;
    }
}

package com.mindvalley_hassan_karimi_android_test.Cache.Manager;

/**
 * Created by sepronik on 07/13/2016.
 */
public class Response<T> {

    protected long lastTimeAccess;

    private int size ;

    protected void setSize(int byteLen){
        size = byteLen / 1024 ;
    }

    public int getSize() {
        return size;
    }

    public long getLastTimeAccess() {
        return lastTimeAccess;
    }

    protected void updateLastTimeAccess() {
        this.lastTimeAccess =  System.currentTimeMillis();
    }
}

package com.mindvalley_hassan_karimi_android_test;


import com.mindvalley_hassan_karimi_android_test.Cache.Manager.CacheManager;
import com.mindvalley_hassan_karimi_android_test.Cache.Manager.Response;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by sepronik on 07/13/2016.
 */
public class CacheTest {

    @Test
    public void createCache() throws Exception {
        assertNotNull(new CacheManager());
    }

    @Test
    public void isSetMaxSizeCache() throws Exception {
        CacheManager cache = new CacheManager();
        assertEquals(cache.getMaxSize() ,((int) (Runtime.getRuntime().maxMemory() / 1024) / 8) );
    }

    @Test
    public void isSetMaxSizeSendToInitCache() throws Exception {
        CacheManager cache = new CacheManager(15000);
        assertNotEquals(cache.getMaxSize() , 0);
    }

    @Test
    public void createResponseCache() throws Exception {
        assertNotNull(new Response());
    }



}
